from random import randint

def create_multiplication(min , max):
    val1 = randint(min, max)
    val2 = randint(min, max)
    result = val1 * val2
    return((val1, val2, result));


num  = int(input("Combien d'operation a faire ?"))
mini = int(input("chiffre min :"))
maxi = int(input("chiffre max :"))

question = open("question.txt", "w")
reponse = open("reponse.txt", "w")

allOperation = [ ]
for i in range(num):
    allOperation.append(create_multiplication(mini, maxi))

print(allOperation)
n = 0
for i in allOperation:
    print(n, '- ', str(i[0]), ' X ', str(i[1]), ' =')
    print(n, '- ', i[2])
    op = (str(n) + '- ' + str(i[0]) + ' X ' + str(i[1]) + ' =\n')
    rep = (str(n) + '- ' + str(i[2]) +'\n')
    question.write(op)
    reponse.write(rep)
    
    n += 1

question.close()
reponse.close()
print("End")
